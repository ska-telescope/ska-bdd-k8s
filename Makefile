# include OCI support
include .make/oci.mk

# include k8s make support
include .make/k8s.mk

# include core make support
include .make/base.mk

# define private overrides for above variables in here
-include PrivateRules.mak

_VENV=.venv
_REQUIREMENTS=requirements/k8s-bdd.python.txt
.PHONY: local-init-venv local-test-k8slib
local-init-venv: 
	$(call venv_exec,$(_VENV),pip install -r $(_REQUIREMENTS))

local-test-k8slib:
	$(call venv_exec,$(_VENV),python3 src/bdd/k8slib.py)

BDD_K8S_CMD=pytest -v -spP --junitxml=./build/k8sbdd.xml src/bdd/k8stest.py
local-test-bdd:
	$(call venv_exec,$(_VENV),$(BDD_K8S_CMD))

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef