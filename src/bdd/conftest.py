# -*- coding: utf-8 -*-
#
# This file is part of the SKA PST DSP project.
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE file for more info.

from typing import Any, Optional
import pytest

def pytest_bdd_apply_tag(tag: str, function: Any) -> Optional[bool]:
    if tag == 'todo':
        marker = pytest.mark.skip(reason="Not implemented yet.")
        marker(function)
        return True
    elif tag == 'skip':
        marker = pytest.mark.skip(reason="Marked as skipped.")
        marker(function)
        return True
    else:
        # Fall back to the default behavior of pytest-bdd
        return None
