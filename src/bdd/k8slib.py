#!/usr/bin/env python3
from os import path, getenv, listdir, walk, path, setsid
from kubernetes import client, config, utils
import yaml
from glob import glob

# This sets the root logger to write to stdout (your console).
# Your script/app needs to call this somewhere at least once.
import logging
logging.basicConfig()

# By default the root logger is set to WARNING and all loggers you define
# inherit that value. Here we set the root logger to NOTSET. This logging
# level is automatically inherited by all existing and new sub-loggers
# that do not set a less verbose level.
logging.root.setLevel(logging.INFO)

from time import sleep

# Configs can be set in Configuration class directly or using helper utility
config.load_kube_config()
v1 = client.CoreV1Api()
v1apps = client.AppsV1Api()
v1batch = client.BatchV1Api()
v1rbac = client.RbacAuthorizationV1Api()
k8sclient = client.ApiClient()

class envvars:
    values = {}
    def __init__(self):
        from dotenv import load_dotenv
        # Get environment variables. Sample env vars as follows
        #   PROJECT=ska-bdd-k8s
        #   K8S_CHART_VALUES=smrb-valid.yaml
        #   K8S_CHART_VALUES_PATH=build
        #   K8S_CHART_PARAMS='demo --values=tests/bdd/${K8S_CHART_VALUES}'
        #   K8S_CHART_PATH=tests/bdd/${PROJECT}
        envar_list = [
            "PWD",
            "K8S_CHART_VALUES",
            "K8S_CHART_VALUES_PATH",
            "K8S_CHART_PATH",
            "KUBE_NAMESPACE",
            "PROJECT"
        ]
        load_dotenv()
        for envvar in  envar_list:
            if getenv(envvar) is not None:            
                self.values[envvar] = getenv(envvar)
            else:
                logging.debug(f"{envvar}: {getenv(envvar)}")


class k8s_interpreter:
    k8s_api_delete = {}
    k8s_api_read = {}
    k8s_api_list = {}
    def __init__(self):
        self.k8s_api_delete["ConfigMap"] = v1.delete_namespaced_config_map
        self.k8s_api_read["ConfigMap"] = v1.read_namespaced_config_map
        self.k8s_api_list["ConfigMap"] = v1.list_namespaced_config_map

        self.k8s_api_delete["Job"] = v1batch.delete_namespaced_job
        self.k8s_api_read["Job"] = v1batch.read_namespaced_job
        self.k8s_api_list["Job"] = v1batch.list_namespaced_job

        self.k8s_api_delete["PersistentVolumeClaim"] = v1.delete_namespaced_persistent_volume_claim
        self.k8s_api_list["PersistentVolumeClaim"] = v1.list_namespaced_persistent_volume_claim
        self.k8s_api_read["PersistentVolumeClaim"] = v1.read_namespaced_persistent_volume_claim

        self.k8s_api_delete["Pod"] = v1.delete_namespaced_pod
        self.k8s_api_list["Pod"] = v1.list_namespaced_pod
        self.k8s_api_read["Pod"] = v1.read_namespaced_pod

        self.k8s_api_delete["StatefulSet"] = v1apps.delete_namespaced_stateful_set
        self.k8s_api_list["StatefulSet"] = v1apps.list_namespaced_stateful_set
        self.k8s_api_read["StatefulSet"] = v1apps.read_namespaced_stateful_set

        self.k8s_api_delete["Service"] = v1.delete_namespaced_service
        self.k8s_api_read["Service"] = v1.read_namespaced_service
        self.k8s_api_list["Service"] = v1.list_namespaced_service

        self.k8s_api_delete["ServiceAccount"] = v1.delete_namespaced_service_account
        self.k8s_api_list["ServiceAccount"] = v1.list_namespaced_service_account
        self.k8s_api_read["ServiceAccount"] = v1.read_namespaced_service_account

        self.k8s_api_delete["Role"] = v1rbac.delete_namespaced_role
        self.k8s_api_list["Role"] = v1rbac.list_namespaced_role
        self.k8s_api_read["Role"] = v1rbac.read_namespaced_role

        self.k8s_api_delete["RoleBinding"] = v1rbac.delete_namespaced_role_binding
        self.k8s_api_read["RoleBinding"] = v1rbac.read_namespaced_role_binding
        self.k8s_api_list["RoleBinding"] = v1rbac.list_namespaced_role_binding


def shell(commands):
    import subprocess    
    process = subprocess.Popen(
            commands, 
            stdout=subprocess.PIPE,
            shell=True,
            close_fds=True,
            preexec_fn=setsid
        )
    return process

def checkfiles(filepath, filename):
    loglabel = "checkfiles"
    command = f"ls {filepath}"
    try:
        p = shell(command)
        stdout, stderr = p.communicate()
        formatted_stdout = stdout.decode("utf-8")
        file_list = list(filter(None, str(formatted_stdout).split('\n')))
        assert filename in formatted_stdout
        for file_name in file_list:
            logging.info(f"{loglabel}:filename:{file_name}")
    except Exception as e:
        logging.info(f"{loglabel}:command:{command}")
        logging.info(f"{loglabel}:stdout:{formatted_stdout}")
    logging.info(f"{loglabel}:file_list:{file_list}")
    return file_list

def generatemanifests(chartpath, chart, overridepath, override, buildpath):
    loglabel = "generatemanifests"
    command = f"""
        mkdir -p {buildpath} ; helm template bdd \\
        --output-dir={buildpath} \\
        --values={overridepath}/{override} \\
        {chartpath}/{chart}
    """
    logging.info(f"{loglabel}:command:{command}")
    try:
        p = shell(command)
        stdout, stderr = p.communicate()
        formatted_stdout = stdout.decode("utf-8")
        list_wrote_manifest = list(
                filter(
                    None, str(formatted_stdout).split('\n')
                )
            )
        assert list_wrote_manifest is not None
        for manifest in list_wrote_manifest:
            logging.info(f"{loglabel}:manifest:{manifest}")
    except Exception as e:        
        logging.info(f"{loglabel}:command:{command}")
        logging.info(f"{loglabel}:stdout:{formatted_stdout}")
        logging.info(f"{loglabel}:list_wrote_manifest:{list_wrote_manifest}")
    return list_wrote_manifest

def getmanifests(rendered_manifest_path):
    loglabel = "getmanifests"
    logging.info(f"{loglabel}:rendered_manifest_path:{rendered_manifest_path}")
    list_manifest = [y for x in walk(rendered_manifest_path) for y in glob(path.join(x[0], '*.yaml'))]
    # TODO: assert existence of individual manifest files
    logging.info(f"{loglabel}:list_manifest:items:{len(list_manifest)}")
    for manifest in list_manifest:
        logging.info(f"{loglabel}:manifest:{manifest}")

    assert list_manifest is not None
    return list_manifest

def getmanifestobjects(k8smanifests):
    import yaml
    from pathlib import Path
    dict_object = {}
    loglabel = "getmanifestobjects"
    logging.info(f"{loglabel}:k8smanifests:items:{len(k8smanifests)}")
    for manifest in k8smanifests:
        logging.info(f"{loglabel}:k8smanifests:manifest:{manifest}")
        chart = manifest.split('/')[1]
        dict_object[manifest] = []
        stream = open(manifest, "r")
        dict_yaml_manifest = yaml.load_all(stream, yaml.FullLoader)
        for k8sobject in dict_yaml_manifest:
            if k8sobject is not None:
                # chart = k8sobject['metadata']['labels']['helm.sh/chart']
                kind = k8sobject['kind']
                name = k8sobject['metadata']['name']
                dict_object[manifest].append({'chart': chart, 'kind': kind, 'name': name})
                #logging.info(f"{loglabel}:{manifest}:k8smanifests:k8sobject:items:{len(k8sobject.items())}")
    assert dict_object != {}
    for manifest in dict_object:
        logging.info(f"{loglabel}:dict_object:manifest:{manifest}")
        for k8sobject in dict_object[manifest]:
            logging.info(f"{loglabel}:dict_object:k8sobject:items:{k8sobject.items()}")
    return dict_object

def applymanifests(k8snamespace, manifests):
    loglabel = "applymanifests"
    k8sobjects = { k8snamespace: {} }
    logging.info(f"{loglabel}:k8snamespace:{k8snamespace}")
    for manifest in manifests:
        try:
            logging.info(f"{loglabel}:manifest:{manifest}")
            scheduledk8sobjects = utils.create_from_yaml(
                k8sclient,
                manifest,
                namespace=k8snamespace
            )
            for k8sobjectlist in scheduledk8sobjects:
                for k8sobject in k8sobjectlist:
                    dict_k8sobject = k8sobject.to_dict()
                    kind = dict_k8sobject["kind"]
                    name = dict_k8sobject["metadata"]["name"]
                    namespace = dict_k8sobject['metadata']['namespace']
                    logging.info(f"{loglabel}:k8sobjectlist:namespace:{namespace}")
                    logging.info(f"{loglabel}:k8sobjectlist:kind:{kind}")
                    logging.info(f"{loglabel}:k8sobjectlist:name:{name}")            
                    if kind not in k8sobjects[namespace].keys():
                        k8sobjects[namespace][kind] = []
                        logging.info(f"{loglabel}:k8sobjects:kind:{kind}")
                    k8sobjects[namespace][kind].append(name)
                    logging.info(f"{loglabel}:{namespace}:{kind}:name:{name}")
        except Exception as e:
            logging.debug(f"{loglabel}:manifest:{manifest}")
            logging.warning(f"{loglabel}:exception:{e}")
    return k8sobjects

def getcreatedk8sresources(k8snamespace, k8sobjectslist):
    loglabel = "getcreatedk8sresources"
    k8sinterpreter = k8s_interpreter()
    k8sresources = {}
    k8sresources[k8snamespace] = {}
    for k8smanifest in k8sobjectslist:
        logging.debug(f"{loglabel}:k8sobjectslist:k8smanifest:{k8smanifest}")
        for k8sobject in k8sobjectslist[k8smanifest]:
            logging.debug(f"{loglabel}:k8sobjectslist:k8smanifest:k8sobject:{k8sobject}")
            assert 'kind' in k8sobject.keys()
            assert 'name' in k8sobject.keys()
            k8sresourcetype = k8sobject['kind']
            k8sname = k8sobject['name']
            if k8sresourcetype not in k8sresources[k8snamespace].keys():
                k8sresources[k8snamespace][k8sresourcetype] = []
            assert k8sresourcetype in k8sresources[k8snamespace].keys()
            k8sresources[k8snamespace][k8sresourcetype].append(
                    k8sinterpreter.k8s_api_read[k8sresourcetype](
                    namespace=k8snamespace, 
                    name=k8sname
                )
            )
    for namespace in k8sresources:
        logging.info(f"{loglabel}:k8sresource:namespace:{namespace}")
        for kind in k8sresources[namespace]:
            logging.info(f"{loglabel}:k8sresource:kind:{kind}")
            for k8sresource in k8sresources[namespace][kind]:
                logging.info(f"{loglabel}:k8sresource:{kind}:name:{k8sresource.to_dict()['metadata']['name']}")
    return k8sresources

def checkpodstates(k8snamespace, k8slabel, k8sdesiredstate):
    loglabel = "checkpodstates"
    attempts = 0
    poll = True
    while attempts < 360 and poll == True:
        try:
            attempts += 1
            sleep(1)
            logging.info(f"{loglabel}:attempts:{attempts}")
            logging.info(f"{loglabel}:k8slabel:{k8slabel}")
            logging.info(f"{loglabel}:k8sdesiredstate:{k8sdesiredstate}")
            pod_list = v1.list_namespaced_pod(
                    namespace=k8snamespace, 
                    label_selector=k8slabel
                )
            for pod_details in pod_list.items:
                podstate = pod_details.status.phase
                logging.info(f"{loglabel}:podname:{pod_details.metadata.name}")
                logging.info(f"{loglabel}:podstate:{podstate}")
                assert k8sdesiredstate in podstate
            # Passing through for loop means all assertions passed. Terminate loop
            poll = False
        except Exception as e:
            logging.debug(f"{loglabel}:podstate:{podstate}")
            logging.debug(f"{loglabel}:k8sdesiredstate:{k8sdesiredstate}")
            logging.debug(f"{loglabel}:exception: {e}")
    return pod_list

def getpodlogs(k8spodlist):
    loglabel = "getpodlogs"
    k8spods = k8spodlist.to_dict()['items']
    logging.info(f"{loglabel}:k8spods:len:{len(k8spods)}")
    k8spodlogs = {}
    for k8spod in k8spods:
        k8snamespace = k8spod['metadata']['namespace']
        k8sname = k8spod['metadata']['name']
        logging.info(f"{loglabel}:k8snamespace:{k8snamespace}")
        logging.info(f"{loglabel}:k8sname:{k8sname}")
        if k8snamespace not in k8spodlogs.keys():
            k8spodlogs[k8snamespace] = {}
        try:
            pod_details = v1.read_namespaced_pod(
                namespace=k8snamespace,
                name=k8sname
            )
            contaier_logs = ""
            for container in pod_details.spec.containers:
                container_name = container.to_dict()['name']
                contaier_logs += v1.read_namespaced_pod_log(
                    namespace=k8snamespace,
                    name=k8sname,
                    container=container_name
                )
            k8spodlogs[k8snamespace][k8sname] = contaier_logs
            assert contaier_logs != ""
        except Exception as e:
            logging.info(f"{loglabel}:contaier_logs:e:{e}")
            logging.info(f"{loglabel}:contaier_logs:{contaier_logs}")
    return k8spodlogs

def checkpodlogs(k8spodlogs, logkeyword):
    loglabel = "checkpodlogs"
    k8sfoundlist = {}
    logging.info(f"{loglabel}:logkeyword:{logkeyword}")
    for k8snamespace in k8spodlogs:
        k8sfoundlist[k8snamespace] = []
        logging.info(f"{loglabel}:k8snamespace:{k8snamespace}")
        for k8spodname in k8spodlogs[k8snamespace]:
            if logkeyword in k8spodlogs[k8snamespace][k8spodname]:
                logging.info(f"{loglabel}:k8spodname:{k8spodname} logkeyword found")
                stdout = k8spodlogs[k8snamespace][k8spodname]
                logging.info(f"{loglabel}:{k8spodname}:stdout::\n{stdout}")
                k8sfoundlist[k8snamespace].append({k8spodname:k8spodlogs[k8snamespace][k8spodname]})
    logfoundcount = len(k8sfoundlist.values())
    try:        
        assert logfoundcount != 0
        logging.info(f"{loglabel}:logfoundcount:len:{logfoundcount}")
    except Exception as e:
        logging.info(f"{loglabel}:logfoundcount:e:{e}")
    return k8sfoundlist

def k8scleanup(k8snamespace, k8sobjects):
    loglabel = "k8scleanup"
    k8sinterpreter = k8s_interpreter()
    k8sresources = {}
    for k8snamespace in k8sobjects:
        logging.info(f"{loglabel}:namespace:{k8snamespace}")
        k8sresources[k8snamespace] = {}
        # NOTE: Cleanup maybe required to be executed in a sequential manner
        for k8sresourcetype in k8sobjects[k8snamespace]:
            if k8sresourcetype not in k8sresources[k8snamespace].keys():
                k8sresources[k8snamespace][k8sresourcetype] = {}
            logging.info(f"{loglabel}:resource:{k8sresourcetype}")
            for k8sobject in k8sobjects[k8snamespace][k8sresourcetype]:
                k8sname = k8sobject.to_dict()['metadata']['name']
                logging.info(f"{loglabel}:k8sname:{k8sname}")
                try:
                    k8sresources[k8snamespace][k8sresourcetype][k8sname] = k8sinterpreter.k8s_api_delete[k8sresourcetype](
                        namespace=k8snamespace, 
                        name=k8sname
                    )
                except:
                    logging.info(f"{loglabel}:k8sname:{k8sname}: failed to delete")
                    logging.info(f"{loglabel}:{k8snamespace}:{k8sresourcetype}:{k8sname}")
    return k8sresources

def getnamespacedresources(k8snamespace):
    loglabel = "getdeletedresources"
    k8sinterpreter = k8s_interpreter()
    k8sresourcelist = {}
    logging.info(f"{loglabel}:namespace:{k8snamespace}")
    k8sresourcelist[k8snamespace] = {}
    for k8skind in k8sinterpreter.k8s_api_list.keys():
        k8sresourcelist[k8snamespace][k8skind] = []
        logging.info(f"{loglabel}:k8skind:{k8skind}")
        k8snamespacedresources = k8sinterpreter.k8s_api_list[k8skind](namespace=k8snamespace).to_dict()
        for k8sitem in k8snamespacedresources['items']:
            k8sname = k8sitem['metadata']['name']
            logging.info(f"{loglabel}:k8sname:{k8sname}")
            k8sresourcelist[k8snamespace][k8skind].append(k8sname)
    return k8sresourcelist

def cleanmanifests(buildpath):
    loglabel = "cleanmanifests"
    command = f"rm -rf {buildpath}; ls"
    try:
        p = shell(command)
        stdout, stderr = p.communicate()
        assert buildpath not in stdout.decode("utf-8")
        file_list = filter(None, str(stdout.decode('utf-8')).split('\n'))
        for filename in file_list:
            logging.info(f"{loglabel}:filename:{filename}")
    except Exception as e:
        logging.info(f"{loglabel}:command:{command}")
        logging.info(f"{loglabel}:stdout:{stdout.decode('utf-8')}")

def main():
    handle = "my-app"
    logger = logging.getLogger(handle)

    # Load env vars used for k8s runtime
    envvar_list = envvars()
    PWD = envvars.values["PWD"]
    K8S_CHART_VALUES = envvars.values["K8S_CHART_VALUES"]
    K8S_CHART_VALUES_PATH = envvars.values["K8S_CHART_VALUES_PATH"]
    k8snamespace = envvars.values["KUBE_NAMESPACE"]
    k8sproject = envvars.values["PROJECT"]
    K8S_CHART = envvars.values["PROJECT"]
    K8S_CHART_PATH = envvars.values["K8S_CHART_PATH"]
    rendered_manifest_path = "./build"
    # Sample static data for localised testing
    list_charts = checkfiles(K8S_CHART_PATH, K8S_CHART)
    list_manifest = generatemanifests(K8S_CHART_PATH,K8S_CHART,K8S_CHART_VALUES_PATH,K8S_CHART_VALUES, rendered_manifest_path)
    manifests = getmanifests(rendered_manifest_path)
    k8sobjectslist = getmanifestobjects(manifests)
    k8sobjectscreate = applymanifests(k8snamespace, manifests)
    k8sobjectscreated = getcreatedk8sresources(k8snamespace, k8sobjectslist)
    k8spodlist = checkpodstates(k8snamespace, "bdd-test=ska-pst-smrb", "Running")
    k8spodlogs = getpodlogs(k8spodlist)
    checkpodlogs(k8spodlogs,"Initializing data and weights ring buffers")
    checkpodlogs(k8spodlogs,"Initializing data and weights stats")
    checkpodlogs(k8spodlogs,"Creating data and weights ring buffers")
    k8sdeletedobjects = k8scleanup(k8snamespace, k8sobjectscreated)
    k8snamespaceddobjects = getnamespacedresources(k8snamespace)
    cleanmanifests("./build")


if __name__ == '__main__':
    main()