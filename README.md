# ska-bdd-k8s
Sample repository acting as library for template python k8s scripts

## Motivation
To create templated bdd scripts for testing the following Helm chart deployment stages:
 - Generate manifest based on one or more Helm chart values.yaml override payload.
 - k8s deployment using generated manifest files.
 - Expected Pod states based on declared chart configuration.
 - Expected keywords present in container logs
 - Helm chart cleanup

## Key outcomes
- Reduction of k8s BDD test code replication.
- Generic k8s deployment lifecycle tests abstracted from application repositories.

## Usage
- Import the repository as a submodule
- Invoke the templated scripts using the `feature file` as a parameter.
- Application specific tests are abstracted using Gherkin [Data Tables](https://cucumber.io/docs/gherkin/reference/#data-tables)

## Limitations
- Each feature file represents a Helm chart values.yaml override payload to avoid k8s resource collision.
- Gherkin feature files must use the provided templates where values are replaced for application specific tests
