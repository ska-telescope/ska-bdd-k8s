from .k8slib import (
    shell,
    envvars,
    checkfiles,
    generatemanifests,
    getmanifestobjects,
    getmanifests,
    applymanifests,
    getcreatedk8sresources,
    checkpodstates,
    getpodlogs,
    checkpodlogs,
    k8scleanup,
    getnamespacedresources,
    cleanmanifests
)

import pytest
from pytest_bdd import given, when, scenarios, then, parsers
scenarios('features')

from time import sleep

@pytest.fixture
def k8spathfiles():
    return []

@pytest.fixture
def k8smanifests():
    return []

@pytest.fixture
def k8senvvars():
    return {}

@pytest.fixture
def k8sobjects():
    return {}

@pytest.fixture
def k8screatedobjects():
    return {}

@pytest.fixture
def k8spodlist():
    return {}

@pytest.fixture
def k8spodlogs():
    return ""

@pytest.fixture
def k8sdeletedobjects():
    return {}

"""
@given
@when
@then
"""

@given(parsers.parse('path {chartpath} contains helm chart {chart}'), target_fixture="k8spathfiles")
def given_chart(chartpath, chart):
    loglabel = "given_chart"
    pathfiles = checkfiles(chartpath, chart)
    print(f"\n{loglabel}:chartpath:{chartpath}")
    print(f"{loglabel}:chart:{chart}")
    print(f"{loglabel}:pathfiles:{pathfiles}")
    assert chart in ' '.join(pathfiles)
    return pathfiles


@given(parsers.parse('path {overridepath} contains override file {override}'))
def given_override(k8spathfiles, overridepath, override):
    loglabel = "given_override"
    pathfiles = checkfiles(overridepath, override)
    print(f"\n{loglabel}:overridepath:{overridepath}")
    print(f"{loglabel}:override:{override}")
    print(f"{loglabel}:pathfiles:{pathfiles}")
    assert override in ' '.join(pathfiles)


@when(parsers.parse('helm chart {chart} in {chartpath} is rendered using {override} in {overridepath} to {buildpath}'), target_fixture="k8smanifests")
def when_chart_render(chart, chartpath, override, overridepath, buildpath):
    loglabel = "when_chart_render"
    print(f"\n{loglabel}:chart:{chart}")
    print(f"{loglabel}:chartpath:{chartpath}")
    print(f"{loglabel}:override:{override}")
    print(f"{loglabel}:overridepath:{overridepath}")
    print(f"{loglabel}:buildpath:{buildpath}")    
    manifests = generatemanifests(
        chartpath,chart,overridepath,override, buildpath
    )
    assert manifests is not None
    for created_manifest in manifests:
        print(f"{loglabel}:created_manifest:{created_manifest}")

    return manifests


@then(parsers.parse('helm chart {chart} rendered manifests exists in {buildpath}'))
def then_manifest_exist(chart, buildpath, k8smanifests):
    loglabel = "then_manifest_exist"
    print(f"\n{loglabel}:chart:{chart}")
    print(f"{loglabel}:buildpath:{buildpath}")
    manifests = getmanifests(buildpath)
    for manifest in list(k8smanifests):
        assert manifest.replace('wrote ', '') in ' '.join(manifests)
        print(f"{loglabel}:manifest:{manifest}")


@given(parsers.parse('manifests exists in {buildpath}'), target_fixture="k8smanifests")
def given_manifest_exist(buildpath):
    loglabel = "given_manifest_exist"
    print(f"\n{loglabel}:buildpath:{buildpath}")
    manifests = getmanifests(buildpath)
    assert manifests != []
    for manifest in manifests:
        print(f"{loglabel}:manifest:{manifest}")
    return manifests


@given(parsers.parse('environment variable for k8s namespace {envvar} is defined'), target_fixture="k8senvvars")
def given_namespace_envvar_is_defined(envvar):
    loglabel = "given_namespace_envvar_is_defined"
    print(f"\n{loglabel}:envvar:{envvar}")
    envvar_list = envvars()
    print(f"{loglabel}:envvar_list:{envvar_list}")
    assert envvar in " ".join(list(envvar_list.values))
    print(f"{loglabel}:namespace:{envvar_list.values[envvar]}")
    return envvar_list


@when(parsers.parse('manifests in {buildpath} gets deployed'), target_fixture="k8sobjects")
def when_manifests_are_deployed(buildpath, k8senvvars, k8smanifests):
    loglabel = "when_manifests_are_deployed"
    k8snamespace = k8senvvars.values["KUBE_NAMESPACE"]
    print(f"\n{loglabel}:namespace:{k8snamespace}")
    assert k8smanifests != []
    for manifest in k8smanifests:
        print(f"{loglabel}:manifest:{manifest}")
    k8sobjectscreate = applymanifests(k8snamespace, k8smanifests)
    for namespace in k8sobjectscreate:
        assert k8sobjectscreate[namespace] != {}
        print(f"{loglabel}:k8sobjects:items{len(k8sobjectscreate[namespace].items())}")
    k8sobjectslist = getmanifestobjects(k8smanifests)
    return k8sobjectslist


@then(parsers.parse('kubernetes resources are created in the namespace defined in the environment variable {envvar}'), target_fixture="k8screatedobjects")
def then_kubernetes_resources_are_created(envvar, k8sobjects, k8senvvars):
    loglabel = "then_kubernetes_resources_are_created"
    k8snamespace = k8senvvars.values["KUBE_NAMESPACE"]
    print(f"\n{loglabel}:k8snamespace:{k8snamespace}")
    for namespace in k8sobjects:
        assert k8sobjects[namespace] != {}
        print(f"{loglabel}:k8sobjects:{namespace}:items:{len(k8sobjects[namespace])}")
    k8screatedobjects = getcreatedk8sresources(k8snamespace, k8sobjects)
    for namespace in k8screatedobjects:
        assert k8screatedobjects[namespace] != {}
        print(f"{loglabel}:k8screatedobjects:{namespace}:items:{len(k8screatedobjects[namespace])}")
    return k8screatedobjects


@given(parsers.parse('kubernetes resources are created in the namespace defined in the environment variable {envvar}'), target_fixture="k8screatedobjects")
def given_kubernetes_resources_are_created(envvar, k8senvvars, k8smanifests):
    loglabel = "given_kubernetes_resources_are_created"
    k8senvvars = envvars()
    assert k8senvvars != {}
    k8snamespace = k8senvvars.values["KUBE_NAMESPACE"]
    print(f"\n{loglabel}:k8snamespace:{k8snamespace}")
    assert k8senvvars is not None
    assert k8smanifests != []
    for manifest in k8smanifests:
        print(f"{loglabel}:manifest:{manifest}")
    k8sobjectslist = getmanifestobjects(k8smanifests)
    assert k8sobjectslist != []
    print(f"{loglabel}:k8sobjectslist:len:{len(k8sobjectslist)}")
    k8sobjectscreated = getcreatedk8sresources(k8snamespace, k8sobjectslist)
    assert k8sobjectscreated != {}
    return k8sobjectscreated


@when("pods exist")
def when_pod_exists(k8screatedobjects):
    loglabel = "when_pod_exists"
    assert k8screatedobjects != []
    print(f"\n{loglabel}:k8screatedobjects:len:{len(k8screatedobjects)}")
    for k8snamespace in k8screatedobjects:
        print(f"{loglabel}:k8snamespace:{k8snamespace}")
        k8sobjects = k8screatedobjects[k8snamespace]
        assert 'Pod' in k8sobjects.keys()
        k8spodlist = k8sobjects['Pod']
        for pod in k8spodlist:
            print(f"{loglabel}:k8sobjects:pod{pod.to_dict()['metadata']['name']}")

@when("jobs exist")
def when_job_exists(k8screatedobjects):
    loglabel = "when_job_exists"
    assert k8screatedobjects != []
    print(f"\n{loglabel}:k8screatedobjects:len:{len(k8screatedobjects)}")
    for k8snamespace in k8screatedobjects:
        print(f"{loglabel}:k8snamespace:{k8snamespace}")
        k8sobjects = k8screatedobjects[k8snamespace]
        assert 'Job' in k8sobjects.keys()
        k8sjoblist = k8sobjects['Job']
        for pod in k8sjoblist:
            print(f"{loglabel}:k8sobjects:pod{pod.to_dict()['metadata']['name']}")

@then(parsers.parse('pods in namespace {envvar} with label {k8slabel} are in a {podstate} state'))
def then_pods_with_label_are_in_expected_state(envvar, k8slabel, podstate):
    loglabel = "then_pods_with_label_are_in_expected_state"
    print(f"\n{loglabel}:k8slabel:{k8slabel}")
    print(f"{loglabel}:podstate:{podstate}")
    k8senvvars = envvars()
    assert k8senvvars.values != {}
    k8snamespace = k8senvvars.values[envvar]
    print(f"{loglabel}:k8snamespace:{k8snamespace}")
    k8spodlist = checkpodstates(k8snamespace, k8slabel, podstate)
    assert k8spodlist is not None
    print(f"{loglabel}:k8spodlist:len:{len(k8spodlist.to_dict()['items'])}")
    for k8spod in k8spodlist.to_dict()['items']:
        name = k8spod['metadata']['name']
        phase = k8spod['status']['phase']
        assert podstate == phase
        print(f"{loglabel}:k8spod:name:{name}")
        print(f"{loglabel}:k8spod:phase:{phase}")


@given(parsers.parse('pods in the namespace present in the env var {envvar} with label {k8slabel} are in a {podstate} state'),target_fixture="k8spodlist")
def given_pods_with_label_are_in_expected_state(envvar,k8slabel,podstate):
    loglabel = "given_pods_with_label_are_in_expected_state"
    print(f"\n{loglabel}:envvar:{envvar}")
    print(f"{loglabel}:k8slabel:{k8slabel}")
    print(f"{loglabel}:podstate:{podstate}")
    k8senvvars = envvars()
    assert k8senvvars.values != {}
    k8snamespace = k8senvvars.values[envvar]
    print(f"{loglabel}:k8snamespace:{k8snamespace}")
    k8spodlist = checkpodstates(k8snamespace, k8slabel, podstate)
    assert k8spodlist is not None
    print(f"{loglabel}:k8spodlist:len:{len(k8spodlist.to_dict()['items'])}")
    for k8spod in k8spodlist.to_dict()['items']:
        name = k8spod['metadata']['name']
        phase = k8spod['status']['phase']
        assert podstate == phase
        print(f"{loglabel}:k8spod:name:{name}")
        print(f"{loglabel}:k8spod:phase:{phase}")
    return k8spodlist


@when(parsers.parse('pods logs are not empty'), target_fixture="k8spodlogs")
def when_pods_logs_are_not_empty(k8spodlist):
    loglabel = "when_pods_logs_are_not_empty"
    k8spods = k8spodlist.to_dict()['items']
    print(f"\n{loglabel}:k8spods:len:{len(k8spods)}")
    k8spodlogs = getpodlogs(k8spodlist)
    for k8snamespace in k8spodlogs:
        print(f"{loglabel}:k8snamespace:{k8snamespace}")
        for k8spod in k8spodlogs[k8snamespace]:
            assert k8spodlogs[k8snamespace][k8spod] != ""
    return k8spodlogs


@then(parsers.parse('pod logs must contain the keyword(s) {logkeywords}'))
def then_pod_logs_contain_keyword(logkeywords,k8spodlogs):
    loglabel = "then_pod_logs_contain_keyword"
    for k8snamespace in k8spodlogs:
        print(f"\n{loglabel}:k8snamespace:{k8snamespace}")
        podlist = k8spodlogs[k8snamespace].keys()
        for k8spodname in podlist:
            print(f"{loglabel}:k8spodname:{k8spodname} logkeyword found")
            stdout = k8spodlogs[k8snamespace][k8spodname]
            print(f"{loglabel}:{k8spodname}:stdout::\n{stdout}")
    k8sfoundlist = checkpodlogs(k8spodlogs,logkeywords)
    logfoundcount = len(k8sfoundlist.values())
    assert logfoundcount != 0



# @given(parsers.parse('manifests exists in {buildpath}'))
# @given(parsers.parse('kubernetes resources are created in the namespace defined in the environment variable {envvar}'))
@when(parsers.parse('kubernetes resources are scheduled for deletion'), target_fixture="k8sdeletedobjects")
def when_kubernetes_resources_are_scheduled_for_deletion(k8smanifests, k8screatedobjects):
    loglabel = 'when_kubernetes_resources_are_scheduled_for_deletion'
    k8senvvars = envvars()
    assert k8senvvars != {}
    k8snamespace = k8senvvars.values["KUBE_NAMESPACE"]
    k8sdeletedobjects = k8scleanup(k8snamespace, k8screatedobjects)
    assert k8sdeletedobjects != []
    for k8snamespace in k8sdeletedobjects:
        assert k8sdeletedobjects[k8snamespace] != []
        for k8skind in k8sdeletedobjects[k8snamespace]:
            assert k8sdeletedobjects[k8snamespace][k8skind] != []
            for k8sname in k8sdeletedobjects[k8snamespace][k8skind]:
                print(f"{loglabel}:{k8skind}:{k8sname}")
    return k8sdeletedobjects

@then(parsers.parse('the namespace present in the env var {envvar} is empty of kubernetes resources'))
def the_namespace_is_empty_of_kubernetes_resources(envvar, k8sdeletedobjects):
    loglabel = 'the_namespace_is_empty_of_kubernetes_resources'
    for k8snamespace in k8sdeletedobjects:
        k8snamespaceddobjects = getnamespacedresources(k8snamespace)
        for k8skind in k8sdeletedobjects[k8snamespace]:
            print(f"{loglabel}:k8sobject:k8skind:{k8skind}")
            k8sobjects = k8sdeletedobjects[k8snamespace][k8skind]
            for k8sname in k8sobjects:
                k8snamespaceddobjectlist = k8snamespaceddobjects[k8snamespace][k8skind]
                while True:
                    timeout = 0
                    try:
                        sleep(1)
                        assert k8sname not in k8snamespaceddobjectlist
                        print(f"{loglabel}:k8sobject:k8sname:{k8skind}:{k8sname} deleted in {k8snamespace}")
                        break
                    except Exception as e:
                        timeout += 1
                        print(f"{loglabel}:k8sobject:k8sname:{k8skind}:{k8sname} in namespace {k8snamespace} pending for deletion")
                        k8snamespaceddobjects = getnamespacedresources(k8snamespace)
                        k8snamespaceddobjectlist = k8snamespaceddobjects[k8snamespace][k8skind]
                        if timeout == 120:
                            break
                        pass
